/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import clicktours.Ciudades;
import clicktours.ClickTours;
import clicktours.Hoteles;
import clicktours.Provincias;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Alcivar S., Chavez L., Nuñez M.
 */
public class BusquedaPorProvinciaYCiudad {
    
    private VBox root = new VBox();
    private HBox panelProvincia = new HBox();
    private Label lblProvincia = new Label("Provincia:");
    private ComboBox<Provincias> provincias= new ComboBox<>();
    private HBox panelDescripcion = new HBox();
    private Label lblProvDescripcion = new Label();
    private HBox panelCiudades = new HBox();
    private Label lblCiudad = new Label("Ciudad:");
    private ComboBox<Ciudades>Ciudades= new ComboBox<>();
    private FlowPane panelHoteles = new FlowPane(Orientation.HORIZONTAL);
    private ScrollPane panelDeslizable = new ScrollPane();
    private HBox panelBusHotel = new HBox();
    private TextField ingresoHotel = new TextField();
    private Button busqueda = new Button("Buscar");
    
    /** Constructor de la clase BusquedaPorProvinciaYCiudad
     * No recibe parametros
     */
    public BusquedaPorProvinciaYCiudad(){
        organizarControles();
        llenarComboBoxPro();
    }
    
    /** Método para llenar el Combo Box
     * No recibe parámetros
     */
    public void llenarComboBoxPro(){
        provincias.getItems().addAll(ClickTours.provincias);
        //SetOnAction del combobox Provincia
        provincias.setOnAction(e-> mostrarProvincia());
    }

    /** Método para organizar los controles
     * No recibe parámetros
     */
    public void organizarControles(){
        panelHoteles.setHgap(450);
        panelHoteles.setVgap(40);
        panelDeslizable.setContent(panelHoteles);
        root.setSpacing(15);
        panelProvincia.getChildren().addAll(lblProvincia,provincias);
        panelDescripcion.getChildren().add(lblProvDescripcion);
        panelCiudades.getChildren().addAll(lblCiudad,Ciudades);
        panelBusHotel.getChildren().addAll(new Label("¿Deseas buscar un hotel por un sitio turístico? ¡Ingrésalo!: "),ingresoHotel,busqueda);
        //panel principal
        root.getChildren().addAll(panelProvincia,panelDescripcion,panelCiudades,panelBusHotel,panelHoteles,panelDeslizable);
    }
    
    /** Método para mostrar la Provincia
     * No recibe parámetros
     */
    public void mostrarProvincia(){
        Provincias p1 = provincias.getValue();
        //descripcion
        lblProvDescripcion.setText(p1.getDescripcion());
        lblProvDescripcion.setWrapText(true);
        //ciudades por provincia
        Ciudades.getItems().clear();
        Ciudades.setOnAction(e->mostrarHoteles());
        for (Ciudades c: ClickTours.ciudades){
            if (p1.getIdProvincia() == c.getIdprovincias()){
                Ciudades.getItems().add(c); 
            }
        }
    }
    
    /** Método para mostrar los Hoteles
     * No recibe parámetros
     */
    
    public void mostrarHoteles(){
        Ciudades c1 = Ciudades.getValue();
        panelHoteles.getChildren().clear();
        for (Hoteles h1 : ClickTours.hoteles){
            if (c1.getIdciudad() == h1.getId_ciudad()){
                HBox panHotHo = new HBox();
                VBox panHotVe = new VBox();
                FlowPane panBoto = new FlowPane(Orientation.HORIZONTAL);
                panBoto.setHgap(20);
                panBoto.setVgap(50);
                Button b = new Button("Informacion");
                Button b1 = new Button("Ver Mapa");
                panBoto.getChildren().addAll(b,b1);
                b.setOnAction(e->presentarPantallaHotel(h1));
                b1.setOnAction(e->presentarMapa(h1));
                ImageView ih = new ImageView("/posters/"+h1.getNombre_hotel()+".jpg");
                ih.setFitHeight(140);
                ih.setFitWidth(100);
                if (h1.getClasificacion_hotel()==1){
                    ImageView i = new ImageView("/posters/star1.jpg");
                    i.setFitHeight(50);
                    i.setFitWidth(150);
                    panHotVe.getChildren().addAll(new Label(h1.getNombre_hotel()),new Label("Dirección: "+h1.getDireccion_hotel()),i,panBoto);
                } 
                else if (h1.getClasificacion_hotel()==2){
                    ImageView i = new ImageView("/posters/star2.jpg");
                    i.setFitHeight(50);
                    i.setFitWidth(150);
                    panHotVe.getChildren().addAll(new Label(h1.getNombre_hotel()),new Label("Dirección: "+h1.getDireccion_hotel()),i,panBoto);
                }

                else if (h1.getClasificacion_hotel()==3){
                    ImageView i = new ImageView("/posters/star3.jpg");
                    i.setFitHeight(50);
                    i.setFitWidth(150);
                    panHotVe.getChildren().addAll(new Label(h1.getNombre_hotel()),new Label("Dirección: "+h1.getDireccion_hotel()),i,panBoto);
                }
                else if (h1.getClasificacion_hotel()==4){
                    ImageView i = new ImageView("/posters/star4.jpg");
                    i.setFitHeight(50);
                    i.setFitWidth(150);
                    panHotVe.getChildren().addAll(new Label(h1.getNombre_hotel()),new Label("Dirección: "+h1.getDireccion_hotel()),i,panBoto);
                }
                else if (h1.getClasificacion_hotel()==5){
                    ImageView i = new ImageView("/posters/star5.jpg");
                    i.setFitHeight(50);
                    i.setFitWidth(150);
                    panHotVe.getChildren().addAll(new Label(h1.getNombre_hotel()),new Label("Dirección: "+h1.getDireccion_hotel()),i,panBoto);
                }
                
                panHotHo.getChildren().addAll(ih,panHotVe);
                panelHoteles.getChildren().addAll(panHotHo);
            }    
        }  
    }

    /** Método para presentar la pantalla del Hotel
     * @param h variable de tipo Hoteles
     */
    public void presentarPantallaHotel (Hoteles h){
        Stage s = new Stage();
        s.setTitle(h.getNombre_hotel());
        Scene sc = new Scene(new VentanaHotel(h).getRoot(),700,600);
        s.setScene(sc);
        s.show();    
    }

    /** Método para presentar el mapa
     * @param h variable de tipo Hoteles
     */
    public void presentarMapa(Hoteles h){
        URI myURI;
         try {
             String query= h.getLatitud()+","+h.getLongitud();
             
             myURI = new URI("https://www.google.com/maps/search/?api=1&query="+query);
             Desktop.getDesktop().browse(myURI);
     
         } catch (URISyntaxException ex) {
             Logger.getLogger(BusquedaPorProvinciaYCiudad.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
             Logger.getLogger(BusquedaPorProvinciaYCiudad.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    /** Método para obtener el root
     * No recibe parametros
     * @return root variable de tipo VBox
     */
    public VBox getRoot() {
        return root;
    }
     
}