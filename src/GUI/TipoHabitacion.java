/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import clicktours.Catalogo;
import clicktours.ClickTours;
import clicktours.Habitaciones;
import clicktours.Hoteles;
import clicktours.Servicios;
import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Samantha A.
 */

public class TipoHabitacion {
    private String tipoHabitacion;

    /** Constructor de la clase TipoHabitacion
     * @param tipoHabitacion variable de tipo String
     */
    public TipoHabitacion(String tipoHabitacion) {
        this.tipoHabitacion = tipoHabitacion;
    }

    /** Método para obtener el tipo de Habitacion
     * No recibe parámetros
     * @return tipoHabitacion variable de tipo String
     */
    public String getTipoHabitacion() {
        return tipoHabitacion;
    }

    /** Método para cambiar el tipo de Habitacion
     * @param tipoHabitacion variable de tipo String
     */
    public void setTipoHabitacion(String tipoHabitacion) {
        this.tipoHabitacion = tipoHabitacion;
    }

    /** Método toString para obtener el dato tipoHabitacion
     * No recibe parámetros
     * @return tipoHabitacion variable de tipo String
     */
    @Override
    public String toString() {
        return tipoHabitacion;
    }
    
    
}
