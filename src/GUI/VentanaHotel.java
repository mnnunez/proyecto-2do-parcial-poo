/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import clicktours.Catalogo;
import clicktours.ClickTours;
import clicktours.Habitaciones;
import clicktours.Hoteles;
import clicktours.Servicios;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Alcivar S., Chavez L., Nuñez M.
 */
public class VentanaHotel {
    public VBox root = new VBox();
    private Hoteles h;
    private Label lblInfo = new Label();
    private Label lblServicio = new Label();
    private HBox panelInfo = new HBox(); 
    private Label lblTariHab = new Label("*PRECIOS DETALLADOS* \nTipo de Habitación -> Tarifa Sencilla - Tarifa Doble - Tarifa Triple \n");
    private FlowPane panelInfoH = new FlowPane(Orientation.HORIZONTAL); 
    //private VBox servicios = new VBox();
    private Map<Integer,ArrayList<String> > tipoHabitacion;
    private VBox panelTarifa = new VBox();
    private VBox panelTitle = new VBox();
    private VBox panelServicios = new VBox();
    private VBox panServicio = new VBox();
    
    
    /** Constructor de la clase VentanaHotel
     * @param h variable de tipo Hoteles
     */
    
    public VentanaHotel(Hoteles h) {
        this.h = h;
        llenarPantalla();
        imprimirInfo();
        agregarServicios();
    }
    
    /** Método para obtener el tipo de Habitacion
     * No recibe parámetros
     * @return tipoHabitacion variable de tipo Map
     */
    public Map<Integer, ArrayList<String>> getTipoHabitacion() {
        return tipoHabitacion;
    }

    /** Método para cambiar el tipo de Habitacion
     * @param tipoHabitacion variable de tipo Map
     */
    public void setTipoHabitacion(Map<Integer, ArrayList<String>> tipoHabitacion) {
        this.tipoHabitacion = tipoHabitacion;
    }

    /** Método para imprimir la informacion
     * No recibe parámetros
     */
    public void imprimirInfo(){
        for (Habitaciones room: ClickTours.habitaciones){
            ArrayList <String> tipo = new ArrayList<String>();
            tipo.add(room.getTipo_habitacion());
            VBox panInfo = new VBox();
            for(int i=0; i<tipo.size();i++){
                if (h.getId_hotel()== room.getId_hotel()){ 
                    panInfo.getChildren().addAll(new Label(tipo.get(i)+" -> $" +room.getTarifa_sencilla()+ " - $"+room.getTarifa_doble()+" - "+room.getTarifa_triple()));
                    panelTarifa.getChildren().addAll(panInfo);
                }
            }
        } 
    }
    
    /** Método para llenar la pantalla
     * No recibe parámetros
     */
    public void llenarPantalla(){        
        lblInfo.setText("\n*DESCRIPCIÓN* \n"+h.getDescripcion_hotel());
        lblInfo.setWrapText(true);
        lblServicio.setText("*SERVICIOS* \n");
        panelInfo.getChildren().addAll(lblInfo);
        panelTitle.getChildren().addAll(lblTariHab);
        panServicio.getChildren().add(lblServicio);
        root.getChildren().addAll(panelInfo,panelTitle,panelTarifa,panServicio,panelServicios);
        root.setSpacing(8);
    }
    
    /** Método para agregar servicios
     * No recibe parámetros
     */
    public void agregarServicios(){
        for (Servicios s1: ClickTours.servicios){
            if (s1.getId_hotel()== h.getId_hotel()){
                int servicio = s1.getId_servicio();
                for (Catalogo c1 : ClickTours.catalogo){
                    if (servicio == c1.getIdservicio()){
                        panelServicios.getChildren().addAll(new Label (c1.getServicio()));
                    }
                }
            }  
        }
    }
    
    /** Método para obtener el root
     * No recibe parámetros
     * @return root variable de tipo VBox
     */
    public VBox getRoot() {
        return root;
    }        
    
}    
