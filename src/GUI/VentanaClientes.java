/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import clicktours.Ciudades;
import clicktours.Cliente;
import clicktours.Hoteles;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 *
 * @author Alcivar S., Chavez L., Nuñez M.
 */
public class VentanaClientes {
   
    private VBox root = new VBox();
    
    private Label lblTitulo = new Label ("AGENCIA TURÍSTICA "+"CLICK TOURS");
    private Label lblCedula = new Label ("Cédula: ");
    private HBox panelCedula = new HBox();
    private TextField txtEntrada1 = new TextField();
    private Label lblNombre = new Label ("Nombre: ");
    private HBox panelNombre = new HBox();
    private TextField txtEntrada2 = new TextField();
    private Label lblDireccion = new Label ("Dirección: ");
    private HBox panelDireccion = new HBox();
    private TextField txtEntrada3 = new TextField();
    private Label lblCorreo = new Label ("Correo Electrónico: ");
    private HBox panelCorreo = new HBox();
    private TextField txtEntrada4 = new TextField();
    private VBox panelRegistro = new VBox();
    private Button registro = new Button("Registrarse");
    private Button busqueda = new Button("Iniciar búsqueda");
    private Button salir = new Button("Salir");
    
    
    public static ArrayList<Cliente> clientes = new ArrayList<>();
    
    /** Constructor de la clase VentanaClientes
     * No recibe parametros
     */
    public VentanaClientes(){
        organizarControles();
        registro.setOnAction(w->registro());
        serializar();
        salir.setOnAction(e-> salir());
        busqueda.setOnAction(e->presentarPantallaBusquedaPorProvinciaYCiudad());
    }
    
    /** Método para organizar los controles
     * No recibe parámetros
     */
    private void organizarControles(){
        
        root.setSpacing(15);
        root.setAlignment(Pos.TOP_CENTER);
 
        panelCedula.getChildren().addAll(lblCedula,txtEntrada1);
        panelNombre.getChildren().addAll(lblNombre,txtEntrada2);
        panelDireccion.getChildren().addAll(lblDireccion,txtEntrada3);
        panelCorreo.getChildren().addAll(lblCorreo,txtEntrada4);
        
        //agrego elementos en el root 
        root.getChildren().addAll(lblTitulo,panelCedula,panelNombre,panelDireccion,panelCorreo,registro,salir);
    }   
    
    /** Método para el registro
     * No recibe parámetros
     */
    private void registro(){
        String id = txtEntrada1.getText();
        String name = txtEntrada2.getText();
        String address = txtEntrada3.getText();
        String mail = txtEntrada4.getText();
        clientes.add(new Cliente(id,name,address,mail));
        panelRegistro.getChildren().add(new Label("\n**Hola, te has registrado en nuestra agencia turística, ¡Bienvenid@!**"));
        root.getChildren().addAll(panelRegistro,busqueda);
        
    }
    
    /** Método para serializar
     * No recibe parámetros
     */
    public void serializar(){
        try(ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("DatosRegistro.dat"))){
            o.writeObject(clientes);
        }
        catch(IOException e){
            System.out.println("Error en serializar "+e);
        }
    }
    
  
    /** Método para salir
     * No recibe parámetros
     */
    private void salir(){
        Platform.exit();
    }
    
    /** Método para presentar la pantalla de busqueda por provincia y ciudad
     * No recibe parámetros
     */
    public void presentarPantallaBusquedaPorProvinciaYCiudad(){
        Stage s = new Stage();
        s.setTitle("Agencia Turística - Búsqueda Por Provincia Y Ciudad");
        Scene sc = new Scene(new BusquedaPorProvinciaYCiudad().getRoot(),700,600);
        s.setScene(sc);
        s.show();    
    }
    
    /** Método para obtener el root
     * No recibe parámetros
     * @return root variable de ttipo VBox
     */
    public VBox getRoot() {
        return root;
    }
    
    
}
