/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

import GUI.BusquedaPorProvinciaYCiudad;
import GUI.VentanaClientes;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;

import javafx.scene.Scene;

import javafx.stage.Stage;

/**
 *
 * @author Alcivar S., Chavez L., Nuñez M.
 */
public class ClickTours extends Application {
    
    public static List<Catalogo> catalogo = new ArrayList<>();
    public static List<Ciudades> ciudades = new ArrayList<>();
    public static List<Habitaciones> habitaciones = new ArrayList<>();
    public static List<Hoteles> hoteles = new ArrayList<>();
    public static List<Provincias> provincias = new ArrayList<>();
    public static List<Servicios> servicios = new ArrayList<>();
    
    public static void main(String[] args) {
        leerArchivoCatalogo();
        leerArchivoCiudades();
        leerArchivoHabitaciones();
        leerArchivoProvincias();
        leerArchivoServicios();
        leerArchivoHoteles();
        launch();
        
    }
    
    /** Método para leer el archivo Catalogo
     *  No recibe parametros
     */
    private static void leerArchivoCatalogo(){
        try {
            List<String> x = Files.readAllLines(Paths.get("src/recursos/catalogo.csv"));
            for (String str1: x.subList(1,x.size())){
                String [] d = str1.split("\\|");
                catalogo.add(new Catalogo(Integer.parseInt(d[0]),d[1]));
            } 
        } 
        catch (IOException ex) {
            System.out.println("Error al leer Archivo: " +ex);
        }
    }
    
    /** Método para leer el archivo Ciudades
     *  No recibe parametros
     */
    private static void leerArchivoCiudades(){
        try {
            List<String> x = Files.readAllLines(Paths.get("src/recursos/ciudades.csv"));
            for (String str1: x.subList(1,x.size())){
                try{
                String [] d = str1.split("\\|");
                ciudades.add(new Ciudades(Integer.parseInt(d[0]),Integer.parseInt(d[1]),d[2]));
                }
                catch(java.lang.ArrayIndexOutOfBoundsException e){
                    System.out.println("Error de índice: "+e);
                }
            } 
        } 
        catch (IOException ex) {
            System.out.println("Error al leer Archivo: " +ex);
        }
    }
    
    /** Método para leer el archivo Habitaciones
     *  No recibe parametros
     */
    private static void leerArchivoHabitaciones(){
        try {
            List<String> x = Files.readAllLines(Paths.get("src/recursos/habitaciones.csv"));
            for (String str1: x.subList(1,x.size())){
                String [] d = str1.split("\\|");
                habitaciones.add(new Habitaciones(Integer.parseInt(d[0]),Integer.parseInt(d[1]),d[2],d[3],d[4],d[5]));
            } 
        } 
        catch (IOException ex) {
            System.out.println("Error al leer Archivo: " +ex);
        }
    }
    
    /** Método para leer el archivo Hoteles
     *  No recibe parametros
     */
    private static void leerArchivoHoteles(){
        try {
            List<String> x = Files.readAllLines(Paths.get("src/recursos/hoteles1.csv"));
            for (String str1: x.subList(1,x.size())){
                try{
                    String [] d = str1.split("\\|");
                    hoteles.add(new Hoteles(Integer.parseInt(d[0]),Integer.parseInt(d[1]),d[2],d[3],d[4],d[5],d[6],d[7],Integer.parseInt(d[8]),d[9],d[10],d[11]));  
                }catch (java.lang.ArrayIndexOutOfBoundsException e){
                    System.out.println("Error de índice: "+e);
                    
                } 
            } 
        } 
        catch(FileNotFoundException e){
            System.err.println("Archivo no encontrado");
        }
        catch (IOException ex) {
            System.out.println("Error: " +ex);
        }
    }
  
    
    /** Método para leer el archivo Provincias
     *  No recibe parametros
     */
    private static void leerArchivoProvincias(){
        try {
            List<String> x = Files.readAllLines(Paths.get("src/recursos/provincias.csv"));
            for (String str1: x.subList(1,x.size())){
                try{
                    String [] d = str1.split("\\|");
                    provincias.add(new Provincias(Integer.parseInt(d[0]),d[1],d[2],d[3],d[4]));
                }catch (java.lang.ArrayIndexOutOfBoundsException e){
                    System.out.println("Error de índice: "+e);
                }    
            } 
        } 
        catch (IOException ex) {
            System.out.println("Error al leer Archivo: " +ex);
        }
    }
    
    /** Método para leer el archivo Servicios
     *  No recibe parametros
     */
    private static void leerArchivoServicios(){
        try {
            List<String> x = Files.readAllLines(Paths.get("src/recursos/servicios.csv"));
            for (String str1: x.subList(1,x.size())){
                String [] d = str1.split("\\|");
                servicios.add(new Servicios(Integer.parseInt(d[0]),Integer.parseInt(d[1]),Integer.parseInt(d[2]),d[3]));
            } 
        } 
        catch (IOException ex) {
            System.out.println("Error al leer Archivo: " +ex);
        }
    }
    
    /** Método para crear la primera escena
     *  @param primaryStage variable de tipo Stage
     */
    @Override
    public void start(Stage primaryStage) {
        Scene s = new Scene(new VentanaClientes().getRoot(),700,600);
        primaryStage.setTitle("Ventana de Registro");
        primaryStage.setScene(s);
        primaryStage.show();
        
    }

   
    
}
