/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
﻿

 */
package clicktours;

/**
 *
 * @author Gloria
 */
public class Hoteles {
    private int id_hotel;
    private int id_ciudad;
    private String nombre_hotel;
    private String descripcion_hotel;
    private String tarjeta_hotel;
    private String ubicacion_hotel;
    private String direccion_hotel;
    private String web_hotel;
    private int clasificacion_hotel;
    private String foto_hotel;
    private String latitud;
    private String longitud;

    /** Constructor de la clase Provincias
     * @param id_hotel variable de tipo int
     * @param id_ciudad variable de tipo int
     * @param  nombre_hotel variable de tipo String
     * @param descripcion_hotel variable de tipo String
     * @param tarjeta_hotel variable de tipo String
     * @param ubicacion_hotel variable de tipo String
     * @param direccion_hotel variable de tipo String
     * @param web_hotel variable de tipo String
     * @param clasificacion_hotel variable de tipo int
     * @param foto_hotel variable de tipo String
     * @param latitud variable de tipo String
     * @param longitud variable de tipo String
     */
    public Hoteles(int id_hotel, int id_ciudad, String nombre_hotel, String descripcion_hotel, String tarjeta_hotel, String ubicacion_hotel, String direccion_hotel, String web_hotel, int clasificacion_hotel, String foto_hotel, String latitud, String longitud) {
        this.id_hotel = id_hotel;
        this.id_ciudad = id_ciudad;
        this.nombre_hotel = nombre_hotel;
        this.descripcion_hotel = descripcion_hotel;
        this.tarjeta_hotel = tarjeta_hotel;
        this.ubicacion_hotel = ubicacion_hotel;
        this.direccion_hotel = direccion_hotel;
        this.web_hotel = web_hotel;
        this.clasificacion_hotel = clasificacion_hotel;
        this.foto_hotel = foto_hotel;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    /** Método para obtener el id del Hotel
     * No recibe parámetros
     * @return id_hotel variable de tipo int
     */
    public int getId_hotel() {
        return id_hotel;
    }

    /** Método para cambiar el id del Hotel
     * @param id_hotel variable de tipo int
     */
    public void setId_hotel(int id_hotel) {
        this.id_hotel = id_hotel;
    }

    /** Método para obtener el id de la ciudad
     * No recibe parámetros
     * @return id_ciudad variable de tipo int
     */
    public int getId_ciudad() {
        return id_ciudad;
    }

    /** Método para cambiar el id de la ciudad
     * @param id_ciudad variable de tipo int
     */
    public void setId_ciudad(int id_ciudad) {
        this.id_ciudad = id_ciudad;
    }

    /** Método para obtener el nombre del hotel
     * No recibe parámetros
     * @return nombre_hotel variable de tipo String
     */
    public String getNombre_hotel() {
        return nombre_hotel;
    }

    /** Método para cambiar el nombre del hotel
     * @param nombre_hotel variable de tipo String
     */
    public void setNombre_hotel(String nombre_hotel) {
        this.nombre_hotel = nombre_hotel;
    }
    
    /** Método para obtener la descripcion del Hotel
     * No recibe parámetros
     * @return descripcion_hotel variable de tipo String
     */

    public String getDescripcion_hotel() {
        return descripcion_hotel;
    }

    /** Método para cambiar la descripcion del Hotel
     * @param descripcion_hotel variable de tipo String
     */
    public void setDescripcion_hotel(String descripcion_hotel) {
        this.descripcion_hotel = descripcion_hotel;
    }

    /** Método para obtener la tarjeta del hotel
     * No recibe parámetros
     * @return tarjeta_hotel variable de tipo String
     */
    public String getTarjeta_hotel() {
        return tarjeta_hotel;
    }

    /** Método para obtener la tarjeta del hotel
     * @param tarjeta_hotel variable de tipo String
     */
    public void setTarjeta_hotel(String tarjeta_hotel) {
        this.tarjeta_hotel = tarjeta_hotel;
    }

    /** Método para obtener la ubicacion del hotel
     * No recibe parámetros
     * @return ubicacion_hotel variable de tipo String
     */
    public String getUbicacion_hotel() {
        return ubicacion_hotel;
    }

    /** Método para cambiar la ubicacion del hotel
     * @param ubicacion_hotel variable de tipo String
     */
    public void setUbicacion_hotel(String ubicacion_hotel) {
        this.ubicacion_hotel = ubicacion_hotel;
    }

    /** Método para obtener la direccion del hotel
     * No recibe parámetros
     * @return direccion_hotel variable de tipo String
     */
    public String getDireccion_hotel() {
        return direccion_hotel;
    }
    
    /** Método para cambiar la direccion del hotel
     * @param direccion_hotel variable de tipo String
     */

    public void setDireccion_hotel(String direccion_hotel) {
        this.direccion_hotel = direccion_hotel;
    }

    /** Método para obtener la web del hotel
     * No recibe parámetros
     * @return web_hotel variable de tipo String
     */
    public String getWeb_hotel() {
        return web_hotel;
    }

     /** Método para cambiar la web del hotel
     * @param web_hotel variable de tipo String
     */
    public void setWeb_hotel(String web_hotel) {
        this.web_hotel = web_hotel;
    }

    /** Método para obtener la clasificacion del Hotel
     * No recibe parámetros
     * @return clasificacion_hotel variable de tipo int
     */
    public int getClasificacion_hotel() {
        return clasificacion_hotel;
    }

    /** Método para cambiar la clasificacion del Hotel
     * @param clasificacion_hotel variable de tipo int
     */
    public void setClasificacion_hotel(int clasificacion_hotel) {
        this.clasificacion_hotel = clasificacion_hotel;
    }

    /** Método para obtener la foto del Hotel
     * No recibe parámetros
     * @return foto_hotel variable de tipo String
     */
    public String getFoto_hotel() {
        return foto_hotel;
    }

    /** Método para obtener la foto del Hotel
     * @param foto_hotel variable de tipo String
     */
    public void setFoto_hotel(String foto_hotel) {
        this.foto_hotel = foto_hotel;
    }

    /** Método para obtener la latitud
     * No recibe parámetros
     * @return latitud variable de tipo String
     */
    public String getLatitud() {
        return latitud;
    }

    /** Método para cambiar la latitud
     * @param latitud variable de tipo String
     */
    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    /** Método para obtener la longitud
     * No recibe parámetros
     * @return longitud variable de tipo String
     */
    public String getLongitud() {
        return longitud;
    }

    /** Método para cambiar la longitud
     * @param longitud variable de tipo String
     */
    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    /** Método toString para obtener el nombre del hotel
     * No recibe parámetros
     * @return nombre_hotel variable de tipo String
     */
    @Override
    public String toString() {
        return nombre_hotel;
    }

   
    
    
            
}
