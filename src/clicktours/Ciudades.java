/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

/**
 *
 * @author Samantha
 */
public class Ciudades {
   private int idciudad;
   private int idprovincias;
   private String ciudad;

   /** Constructor de la clase Ciudades
     * @param idciudad variable de tipo int
     * @param idprovincias variable de tipo int
     * @param ciudad variable de tipo String
     */
    public Ciudades(int idciudad, int idprovincias, String ciudad) {
        this.idciudad = idciudad;
        this.idprovincias = idprovincias;
        this.ciudad = ciudad;
    }
    
    /** Constructor de la clase Ciudades
     * No recibe parametros
     */
    public Ciudades(){
        
    }
    
    /** Método para obtener el id de la ciudad
     * No recibe parámetros
     * @return idciudad variable de tipo int
     */
    public int getIdciudad() {
        return idciudad;
    }

     /** Método para cambiar el id de la ciudad
     * @param idciudad variable de tipo int
     */
    public void setIdciudad(int idciudad) {
        this.idciudad = idciudad;
    }

    /** Método para obtener el id de las provincias
     * No recibe parámetros
     * @return idprovincias variable de tipo int
     */
    public int getIdprovincias() {
        return idprovincias;
    }

    /** Método para cambiar el id de las provincias
     * @param idprovincias variable de tipo int
     */
    public void setIdprovincias(int idprovincias) {
        this.idprovincias = idprovincias;
    }

    /** Método para obtener la Ciudad
     * No recibe parámetros
     * @return ciudad variable de tipo string
     */
    public String getCiudad() {
        return ciudad;
    }

     /** Método para cambiar la Ciudad
     * @param ciudad varaible de tipo String
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /** Método toString para obtener el dato ciudad
     * No recibe parámetros
     * @return ciduad variable de tipo String
     */
    @Override
    public String toString() {
        return ciudad;
    }
    
   
}
