/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

import java.io.Serializable;

/**
 *
 * @author SamanthaA.
 */
public class Cliente implements Serializable{
    String cedula;
    String nombre;
    String direccion;
    String correo;

    /** Constructor de la clase Cliente
     * @param cedula variable de tipo String
     * @param nombre variable de tipo String
     * @param  direccion variable de tipo String
     * @param correo variable de tipo String
     */
    public Cliente(String cedula, String nombre, String direccion, String correo) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.direccion = direccion;
        this.correo = correo;
    }

    /** Método toString para obtener la informacion del Cliente
     * No recibe parámetros
     */
    @Override
    public String toString() {
        return "Cliente{" + "Cédula=" + cedula + ", Nombre=" + nombre + ", Dirección=" + direccion + ", Correo Electrónico=" + correo + '}';
    }
    
          

}
