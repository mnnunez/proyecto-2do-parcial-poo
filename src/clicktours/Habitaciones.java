/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

/**
 *
 * @author Gloria
 */
public class Habitaciones {
    private int id_habitacion;
    private int id_hotel;
    private String tipo_habitacion;
    private String tarifa_sencilla;
    private String tarifa_doble;
    private String tarifa_triple;

    /** Constructor de la clase Habitaciones
     * @param id_habitacion variable de tipo int
     * @param id_hotel variable de tipo int
     * @param tipo_habitacion variable de tipo String
     * @param tarifa_sencilla variable de tipo String
     * @param tarifa_doble variable de tipo String
     * @param tarifa_triple variable de tipo String
     */
    public Habitaciones(int id_habitacion, int id_hotel, String tipo_habitacion, String tarifa_sencilla, String tarifa_doble, String tarifa_triple) {
        this.id_habitacion = id_habitacion;
        this.id_hotel = id_hotel;
        this.tipo_habitacion = tipo_habitacion;
        this.tarifa_sencilla = tarifa_sencilla;
        this.tarifa_doble = tarifa_doble;
        this.tarifa_triple = tarifa_triple;
    }

    /** Método para obtener el id de la Habitacion
     * No recibe parámetros
     * @return id_habitacion variable de tipo int
     */
    public int getId_habitacion() {
        return id_habitacion;
    }
    
    /** Método para cambiar el id de la Habitacion
     * @param id_habitacion variable de tipo int
     */

    public void setId_habitacion(int id_habitacion) {
        this.id_habitacion = id_habitacion;
    }

    /** Método para obtener el id del Hotel
     * No recibe parámetros
     * @return id_hotel variable de tipo int
     */
    public int getId_hotel() {
        return id_hotel;
    }

    /** Método para cambiar el id del Hotel
     * @param id_hotel variable de tipo int
     */
    public void setId_hotel(int id_hotel) {
        this.id_hotel = id_hotel;
    }

    /** Método para obtener el tipo de habitacion
     * No recibe parámetros
     * @return tipo_habitacion variable de tipo String
     */
    public String getTipo_habitacion() {
        return tipo_habitacion;
    }

    /** Método para cambiar el tipo de habitacion
     * @param tipo_habitacion variable de tipo String
     */
    public void setTipo_habitacion(String tipo_habitacion) {
        this.tipo_habitacion = tipo_habitacion;
    }

    /** Método para obtener la tarifa sencilla
     * No recibe parámetros
     * @return tarifa_sencilla variable de tipo String
     */
    public String getTarifa_sencilla() {
        return tarifa_sencilla;
    }
    
    /** Método para cambiar la tarifa sencilla
     * @param tarifa_sencilla varible de tipo String
     */

    public void setTarifa_sencilla(String tarifa_sencilla) {
        this.tarifa_sencilla = tarifa_sencilla;
    }

    /** Método para obtener la tarifa doble
     * No recibe parámetros
     * @return tarifa_doble variable de tipo String
     */
    public String getTarifa_doble() {
        return tarifa_doble;
    }

    /** Método para cambiar la tarifa doble
     * @param tarifa_doble varible de tipo String
     */
    public void setTarifa_doble(String tarifa_doble) {
        this.tarifa_doble = tarifa_doble;
    }

    /** Método para obtener la tarifa triple
     * No recibe parámetros
     * @return tarifa_triple variable de tipo String
     */
    public String getTarifa_triple() {
        return tarifa_triple;
    }

    /** Método para cambiar la tarifa triple
     * @param tarifa_triple varible de tipo String
     */
    public void setTarifa_triple(String tarifa_triple) {
        this.tarifa_triple = tarifa_triple;
    }
    
    
    
}
