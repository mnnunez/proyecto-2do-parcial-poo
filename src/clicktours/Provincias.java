/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 */
package clicktours;

import java.util.ArrayList;

/**
 *
 * @author Gloria
 */
public class Provincias {
    private int idProvincia;
    private String provincia;
    private String descripcion;
    private String region;
    private String web;
    public static ArrayList<String> ciudades = new ArrayList<>();
    public static ArrayList<Provincias> provincias = new ArrayList<>();

    public static ArrayList<String> getCiudades() {
        return ciudades;
    }
    /** Constructor de la clase Provincias
     * @param idProvincia variable de tipo int
     * @param provincia variable de tipo String
     * @param  descripcion variable de tipo String
     * @param region variable de tipo String
     * @param web variable de tipo String
     */
    public Provincias(int idProvincia, String provincia, String descripcion, String region, String web) {
        this.idProvincia = idProvincia;
        this.provincia = provincia;
        this.descripcion = descripcion;
        this.region = region;
        this.web = web;
    }
    /** Método para obtener el idProvincia
     * No recibe parámetros
     * @return idProvincia variable de tipo int
     */
    public int getIdProvincia() {
        return idProvincia;
    }

    /** Método para cambiar la idProvincia
     * @param idProvincia variable de tipo int
     */
    public void setIdProvincia(int idProvincia) {
        this.idProvincia = idProvincia;
    }

    /** Método para obtener la Provincia
     * No recibe parámetros
     * @return provincia variable de tipo String
     */
    public String getProvincia() {
        return provincia;
    }

    /** Método para cambiar la Provincia
     * @param provincia variable de tipo String
     */
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    /** Método para obtener la descrpcion
     * No recibe parámetros
     * @return descripcion variable de tipo String
     */
    public String getDescripcion() {
        return descripcion;
    }

    /** Método para cambiar la descrpcion
     * @param descripcion variable de tipo String
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /** Método para obtener la Region
     * No recibe parámetros
     * @return region variable de tipo String
     */
    public String getRegion() {
        return region;
    }

    /** Método para cambiar la region
     * @param  region variable de tipo String
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /** Método para obtener la web
     * No recibe parámetros
     * @return web variable de tipo String
     */
    public String getWeb() {
        return web;
    }
    
    /** Método para cambiar la web
     * @param web variable de tipo String
     */

    public void setWeb(String web) {
        this.web = web;
    }
    /** Método toString para obtener el dato provincia
     * No recibe parámetros
     * @return provincia variable de tipo String
     */

    @Override
    public String toString() {
        return provincia;
    }
    
    
}
